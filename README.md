<!--lint disable awesome-list-item-->
<!--lint disable awesome-toc-->
<!--lint disable no-blockquote-without-marker-->

# 国际开源达人 精选的优秀的  **ESP 系列模组** 应用 **资源列表**  [![Awesome](https://awesome.re/badge.svg)](https://awesome.re) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code-of-conduct.md)
精选 ESP8266/32 开源项目和 **开源** 代码列表（代码和库文件链接已从  **GitHub**  替换为国内服务器  **gitee 码云** 链接） :revolving_hearts: .

<a href="http://espressif.com/en/products/hardware/esp8266ex/overview"><img src="img/esp8266.jpg" alt="ESP8266" align="left" style="margin-right: 25px" height=150></a>
<a href="http://espressif.com/en/products/hardware/esp32/overview"><img src="https://pbs.twimg.com/profile_images/863510403120222208/rjVOiTe3.jpg" alt="ESP32" align="left" style="margin-right: 25px" height=150></a>
>  [ESP8266](http://espressif.com/en/products/hardware/esp8266ex/overview) 和 [ESP32](http://espressif.com/en/products/hardware/esp32/overview) 都是由上海 Espressif Systems 公司生产的具有完整 TCP/IP 堆栈和微控器功能的低成本 Wi-Fi 微控芯片.
> <br/>
> 查看 [贡献](contributing.md) 有关如何为此列表做出贡献的信息.
> <br/><br/>
---
<!--lint disable no-repeat-punctuation-->
<!--lint disable remark-lint-double-link-->
## 内容
- [固件](#firmware)
- [工具](#tools)
- [项目](#projects)
  - [智能家居和物联网](#smart-home-and-iot)
  - [信息安全](#infosec)
  - [生物医学](#biomedical)
  - [其他](#others)
- [库](#libraries)

## 固件
- [Espressif AT](http://bbs.espressif.com/) - ESP8266 的默认 vanilla 固件.
- [NodeMCU](https://gitee.com/jun-tian/nodemcu-firmware) - ESP8266 的eLua-based 的固件 .
- [ESPBasic](http://www.esp8266basic.com/) - 用于简易无线编程的 BASIC 固件，可用于 8266.
- [MicroPython](https://gitee.com/jun-tian/micropython_esp) - 在 Python3 为 ESP8266 and 32编程.
- [ESP32](https://gitee.com/jun-tian/ESP3D) - ESP32 和 8266，用于 3D 打印机的实验固件.
- [Frankenstein](https://gitee.com/jun-tian/esp8266-frankenstein) - 一个又快又烂的固件，让ESP8266 有酷炫的功能.
- [MongooseOS](https://gitee.com/jun-tian/Mongoose-OS) - 物联网专用固件，适用于 C 和 JS。可用于 ESP32/8266.
- [DeviceHive](https://devicehive.com/) - 作为 DeviceHive 的 IoT 数据平台的客户端制作的固件，仅适用于 8266.
- [RT-Thread](https://gitee.com/jun-tian/rt-thread) - 可用于 ESP32 的中文开源固件.
- [Sming Framework](https://gitee.com/jun-tian/Sming) - 出色的 C/C++ 物联网框架，支持 ESP8266 和 ESP32.  

## 工具
- [ESP Flash Tool](http://espressif.com/en/support/download/other-tools) - ESP系列 的原厂固件烧录器。
- [Arduino Core/8266](https://gitee.com/jun-tian/Arduino) - ESP8266 的 Arduino 内核 .
- [Arduino Core/32](https://gitee.com/jun-tian/arduino-esp32) - ESP32 的另一个 Arduino 内核.
- [ESPTool](https://gitee.com/jun-tian/esptool) -  Espressif 的命令行工具，用于ESP系列引导程序.
- [ESP-Open-SDK](https://gitee.com/jun-tian/esp-open-sdk) - 用于 ESP8266 的开放式 SDK.
- [ESPTool-ck](https://gitee.com/jun-tian/esptool-ck) - 用于在 ESP8266 中刷机的 CLI 工具.
- [ESPTool-gui](https://gitee.com/jun-tian/esptool-gui) - 基于ESPTool-ck的刷机GUI工具.
- [NodeMCU Flasher](https://gitee.com/jun-tian/nodemcu-flasher) - NodeMCU 的官方刷机工具.
- [LuaNode](https://gitee.com/jun-tian/LuaNode) - 适用于 32/8266 的仅限 lua 的 SDK.
- [Tuya-Convert](https://gitee.com/jun-tian/tuya-convert) -   ESP8266Wi-Fi 刷机固件可被用于预装涂鸦固件.
- [Tasmotizer](https://gitee.com/jun-tian/tasmotizer) - Tasmota 固件的图形刷写工具。可以管理 Wi-Fi 和 MQTT 设置、模块和模板 .
- [Arduino ESP8266 FS Plugin](https://gitee.com/jun-tian/arduino-esp8266fs-plugin) - 在 ESP8266 中用于文件系统上传的 Arduino 插件 .
- [Arduino ESP32 FS Plugin](https://gitee.com/jun-tian/arduino-esp32fs-plugin) - 在 ESP32 中用于文件系统上传的 Arduino 插件 .
- [PlatformIO](https://gitee.com/jun-tian/platformio-core) - 支持 ESP32 和 ESP8266 的跨平台 IDE 和调试器.

## 项目
### 智能家居和IOT
- [OpenMQTTGateway](https://gitee.com/jun-tian/OpenMQTTGateway) - 用于 ESP设备和其他设备间的多协议 MQTT 网关.
- [ESPHome](https://esphome.io/) - ESP模组控制，使用简单强大的配置文件和家庭自动化系统的全功能实现.
- [Tasmota](https://tasmota.github.io/docs/) - Sonoff 和其他 ESP8266/ESP32 设备的替代固件。包括大量传感器驱动程序并与 [Home Assistant](https://www.home-assistant.io/) 原生或通过 MQTT 集成.
- [Sonoff-Homekit](https://gitee.com/jun-tian/Sonoff-Homekit) - Sonoff 设备（和其他 8266 设备）的替代固件，允许通过 Apple 的 Homekit 进行控制.
- [DoorsignEPD](https://gitee.com/jun-tian/DoorsignEPD) - 使用 ESP32 的带有电子墨迹显示屏的智能门牌.
- [EPaperWeatherDisplay](https://gitee.com/jun-tian/esp32-e-paper-weatherdisplay) - 使用 ESP32 的非常可爱的电子墨迹天气显示屏.
- [SuperGreenOS](https://gitee.com/jun-tian/SuperGreenOS) - 适用于 ESP32 的全功能家庭农业自动化软件.
- [CanAirIO](https://gitee.com/jun-tian/canairio_firmware-canairio-firmware) - 使用移动和固定站通过 ESP32 和智能手机测量空气质量的市政环境监测项目.

### 信息安全
- [ESP32-BLECollector](https://gitee.com/jun-tian/ESP32-BLECollector) - 一个侦测和显示周围存在的BLE设备，并从中收集数据，所有这些都显示在一个漂亮的界面中。
- [ESP32Marauder](https://github.com/justcallmekoko/ESP32Marauder) - 一套集成的 WiFi 和蓝牙攻防工具.
- [ArduinoPcap](https://github.com/spacehuhn/ArduinoPcap) - 允许生成 ESP系列芯片网络流量数据 .pcap 文件的库 。
- [WiFi Satellite](https://hackaday.io/project/28831-wifi-satellite-34c3) - 一个巨大的 Wifi “卫星”，可以使用 14 个 ESP32 监控所有 14 个 2.4Ghz 频道 .
- [ESP8266 Deauther](https://github.com/spacehuhn/esp8266_deauther) - 一个非常酷的使用 ESP8266 的 Wifi 网络伪干扰器（deauther）.
- [PacketMonitor](https://github.com/spacehuhn/PacketMonitor32) - 一个漂亮的 OLED 监视器，用于 WiFi 通道中的数据包活动。 有两个版本为每钟ESP用。
- [WiFiDuck](https://github.com/spacehuhn/WiFiDuck) - 一个支持无线的按键注入器，模拟，但比 Rubber Ducky 更棒.
- [ESP8266 Beacon Spam](https://github.com/spacehuhn/esp8266_beaconSpam) - 该应用可创建数多个虚拟 WiFi 网络.
- [DeauthDetector](https://github.com/spacehuhn/DeauthDetector) - 如果检测到 WiFi deauth 攻击，就会发光的小型设备。

### 生物医学
- [HeartyPatch](https://heartypatch.protocentral.com/) - 使用 ESP32 的可穿戴设备 BLE 和 WiFi 连接到 ECG-HR 贴片.
- [HealthyPi v4](https://www.crowdsupply.com/protocentral/healthypi-v4-unplugged) - 一个令人惊讶的开源生命体征监测器，可以监测心电图、呼吸、脉搏血氧饱和度和体温，所有这些都在 ESP32上 运行 .

### LoRa 无线

- [Meshtastic](https://www.meshtastic.org/) - ESP32 LoRA 板作为安全、长寿命电池、mesh GPS 等设备通信 .
- [ESP32-Paxcounter](https://github.com/cyberman54/ESP32-Paxcounter#esp32-paxcounter) Wifi 和蓝牙驱动，支持 LoRaWAN，电池供电的迷你 Paxcounter（人数计数器），建立在廉价的 ESP32 LoRa IoT 板上 
- [Disaster Radio](https://disaster.radio/) - 由太阳能驱动的抗灾通信网络

### 其他
- [Open SmartWatch](https://open-smartwatch.github.io/) - 带有 GPS、惯性装置和超酷 3D 打印表壳的 FOSS 智能手表.
- [SoftRF](https://github.com/lyusupov/SoftRF) - 可用于无人机项目的 DIY 航空近距离感知系统.
- [Retro ESP32](https://github.com/retro-esp32/RetroESP32) - 一个非常酷的 Odroid Go 启动器（带有 ESP32），它允许模拟几个复古控制台 .
- [PedalinoMini](https://github.com/alf45tar/PedalinoMini) - 使用 ESP32 构建的吉他手无线 MIDI 踏板控制器.
- [StickWatch](https://github.com/eggfly/StickWatch) - 基于 M5Stick 的智能手表模块，使用 ESP32 .
- [DroneBridge](https://github.com/DroneBridge/ESP32) - DroneBridge 的实现，ESP32 上无人机和 UAV 的信号链路.
## 库文件
- [Wasm3](https://github.com/wasm3/wasm3) - 专为嵌入式设备设计的光速的 WebAssembly 解释器，与 ESP 兼容 .
- [Homie8266](https://github.com/marvinroger/homie-esp8266) - 8266 的 Homie 协议的框架实现.
- [ESP-Dash](https://github.com/ayushsharma82/ESP-DASH) - 用于在 8266/32 中创建远程仪表板的漂亮而快速的框架。无需互联网 .
- [ESP_mqtt](https://github.com/tuanpmt/esp_mqtt) - ESP8266 的 MQTT 帮助程序库.
- [GUIslice](https://github.com/ImpulseAdventure/GUIslice) - 用于多个设备和屏幕控制器的拖放 GUI 框架。兼容 ESP8266和32.
- [MicroWebSrv2](https://github.com/jczic/MicroWebSrv2) - 一个非常强大的 MicroPython Web 服务器，可以在 ESP32 中使用.
- [IRremoteESP8266](https://github.com/markszabo/IRremoteESP8266) - 在 ESP8266 中发射和接收 IR遥控信号.
- [esphomelib](https://github.com/OttoWinter/esphomelib) - 在 ESP8266 与 HomeAssistant 集成框架 .
- [TTS](https://github.com/jscrane/TTS) - 一个很好的文本转语音库，适用于几个 Arduino 设备，适用于ESP系列.
- [Free802.11](https://github.com/Jeija/esp32free80211) - 使用 ESP32 发射任意 802.11 信号的库.
- [Koyn](https://github.com/elkrem/koyn) - 用于 ESP32 和 ESP8266 的去中心化比特币库.
- [TFTLibrary](https://github.com/loboris/ESP32_TFT_library) - ESP32 的 TFT 兼容性.
- [UTFT-ESP](https://github.com/gnulabis/UTFT-ESP) - ESP32/8266 的 UTFT 支持.
- [ESPAudio](https://github.com/earlephilhower/ESP8266Audio) - 用于在 ESP8266/ESP32 中播放各种音频格式的库.
- [AsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP) - ESP8266 和 32 的异步 TCP 库.
- [ESP-HomeKit](https://github.com/maximkulkin/esp-homekit) - 在 RTOS 上实现 8266 的 Homekit.
- [ESPHelper](https://github.com/ItKindaWorks/ESPHelper) - 用于 8266 的 MQTT 和 Wi-fi 自动化库.
- [ESPHelper/32](https://github.com/ItKindaWorks/ESPHelper32) - 用于 ESP32 的 ESPHelper 库的端口.
- [ESP8266Wifi](https://github.com/ekstrand/ESP8266wifi) - ESP8266 的简单 Arduino Wifi 库.
- [WiFiESP](https://github.com/bportaluri/WiFiEsp) - 用于 Wifi 管理的 Arduino 库，ESP8266 板的客户端/服务器.
- [TinyGSM](https://github.com/vshymanskyy/TinyGSM) - 一个快速简单的 Arduino 库，用于与 GSM 模块交互，也可以通过 AT 命令控制 ESP8266.
- [mJS](https://github.com/cesanta/mjs) - MongooseOS使用的轻量级受限JS引擎，兼容ESP32和8266.
- [ESPUI](https://github.com/s00500/ESPUI) - 一个简单的库，用于为两个 ESP模块 制作交互式 Web 界面.
- [ESP32 ePaper](https://github.com/loboris/ESP32_ePaper_example) - 用于将 ePaper电子墨迹显示屏 模块与 ESP32 一起使用的全功能库.
- [TinyUPnP](https://github.com/ofekp/TinyUPnP) - 用于 ESP8266 和 32 上的自动端口转发的轻量级 UPnP IGD 库 .
- [Esp32SSHClient](https://github.com/J-Rios/Arduino-esp32sshclient) - 在 ESP32 中实现 SSH 客户端的库.
- [painlessMesh](https://github.com/gmag11/painlessMesh) - 一个库，负责使用 ESP8266 和 ESP32 硬件创建简单的网状网络.
- [WifiEspNow](https://github.com/yoursunny/WifiEspNow) -   [ESP-NOW]Arduino库(https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html), 一种不需要网络连接的 WiFi 通信协议，由 [Espressif](https://github.com/espressif).
- [go-mcu](https://github.com/matiasinsaurralde/go-mcu) - 用于与基于 NodeMCU 的板交互的 Golang 包.
- [CanAirIO SensorLib](https://github.com/kike-canaries/canairio_sensorlib#canairio-air-quality-sensors-library) - 自动配置多个 PM2.5、CO2 和环境传感器的 ESP32/8266 库.
- [Dhyara](https://github.com/neel/dhyara) - 一个 C/C++ 库，用于使用 ESP Now 创建移动自组网络 (MANET).
